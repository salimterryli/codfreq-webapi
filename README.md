# codfreq-webapi

Another layer of wrapper on top of [codfreq](https://github.com/hivdb/codfreq), which provides HTTP access for codfreq in a stable container instead of original dynamically-created container behavior.

Corresponding version of `fastq2codfreq` is provided with minor usage midification.
