import asyncio
import datetime
import json
import os.path
import uuid
import tempfile
import aiofile
from fastapi import FastAPI
from fastapi import HTTPException
from fastapi import UploadFile
# from fastapi.responses import StreamingResponse
from fastapi.responses import FileResponse

app = FastAPI()

session_storage = {}
session_storage_lock = asyncio.Lock()


class BackgroundRunner:
    async def run_main(self):
        while True:
            try:
                await asyncio.sleep(60 * 60)
            except asyncio.CancelledError:
                break

            async with session_storage_lock:
                for session in list(session_storage):
                    async with session_storage[session]["lock"]:
                        if datetime.datetime.now() - session_storage[session]["last_accessed"]\
                                > datetime.timedelta(hours=6):
                            del session_storage[session]


@app.on_event('startup')
async def app_startup():
    runner = BackgroundRunner()
    asyncio.create_task(runner.run_main())


@app.get("/")
async def root():
    return {"message": "Hello World! cod2freq daemon"}


@app.get("/session")
async def create_new_session():
    async with session_storage_lock:
        while True:
            new_uuid = str(uuid.uuid4())
            if new_uuid not in session_storage:
                session_storage[new_uuid] = {
                    "lock": asyncio.Lock(),  # avoid session deletion while session in use
                    "workdir": tempfile.TemporaryDirectory(),
                    "last_accessed": datetime.datetime.now()
                }
                return {
                    "session_id": new_uuid
                }
            else:
                continue


@app.delete("/session/{session_id}")
async def delete_session(session_id: str):
    async with session_storage_lock:
        if session_id not in session_storage:
            raise HTTPException(status_code=404)
    async with session_storage[session_id]["lock"]:
        session_storage[session_id]["workdir"].cleanup()
        del session_storage[session_id]
    return


@app.post("/session/{session_id}/upload")
async def session_upload_fastq(session_id, files: list[UploadFile]):
    async with session_storage_lock:
        if session_id not in session_storage:
            raise HTTPException(status_code=404)
        session_storage[session_id]["last_accessed"] = datetime.datetime.now()
    async with session_storage[session_id]["lock"]:
        processed_files = []
        for fn in files:
            async with aiofile.async_open(
                    os.path.join(session_storage[session_id]["workdir"].name, fn.filename),
                    'bw') as afp:
                await afp.write(await fn.read())
            processed_files.append(fn.filename)
        return {
            "files": processed_files
        }


@app.post("/session/{session_id}/process")
async def session_process(session_id, profile: dict, disable_autopair: bool = False):
    async with session_storage_lock:
        if session_id not in session_storage:
            raise HTTPException(status_code=404)
        session_storage[session_id]["last_accessed"] = datetime.datetime.now()
    async with session_storage[session_id]["lock"]:
        profile_json_file = os.path.join(session_storage[session_id]["workdir"].name, "profile.json")
        async with aiofile.async_open(profile_json_file, 'w') as afp:
            await afp.write(json.dumps(profile))
        proc = await asyncio.create_subprocess_exec(
            "bin/align-all-local",
            "-r", profile_json_file,
            "-d", session_storage[session_id]["workdir"].name,
            "-m" if disable_autopair else "",
            cwd="/app",
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE
        )
        stdout, stderr = await proc.communicate()  # TODO: 改写为 StreamResponse 实时回传stdout与stderr
        return {
            "ret": proc.returncode,
            "stdout": stdout.decode(),
            "stderr": stderr.decode(),
            "files": os.listdir(session_storage[session_id]["workdir"].name)
        }


@app.get("/session/{session_id}/{filename}", response_class=FileResponse)
async def session_download_file(session_id, filename):
    async with session_storage_lock:
        if session_id not in session_storage:
            raise HTTPException(status_code=404)
        session_storage[session_id]["last_accessed"] = datetime.datetime.now()
    async with session_storage[session_id]["lock"]:
        filename = os.path.join(session_storage[session_id]["workdir"].name, filename)
        if os.path.isfile(filename):
            return filename
        else:
            raise HTTPException(status_code=404)
