FROM docker.io/hivdb/codfreq-runner:latest

COPY . /daemon
RUN pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN cd /daemon && python -m venv venv && source venv/bin/activate && \
    pip install -r requirements.txt
ENV PATH="/daemon/venv/bin:$PATH"

ENTRYPOINT /daemon/entry.sh
