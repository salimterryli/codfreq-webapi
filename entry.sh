#!/usr/bin/env sh

cd /daemon
uvicorn main:app --host 0.0.0.0
